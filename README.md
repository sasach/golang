# The Go Programming Language

## Getting started
###### Enable dependency tracking for the code - when the code imports packages contained in other modules, the dependencies are managed through the code's own module. `go.mod` file is needed to define and track them. `go mod init` command helps achieve this.
```
go mod init gitlab.com/sasach/golang
```
###### Declare a package
```
package main
```
###### Import a package
```
import "package_name"
```
###### Use the Println function
```
import "fmt"

fmt.Println()
```
###### Run a Go program
```
go run filename.go
```
###### List of all Go commands
```
go help
```
###### Call code in an external package
```
import "source/package_name"

func main() {
	package_name.included_function()
}
```
###### Add new module requirements before execution
```
go mod tidy
```
###### Variables
```
var a string = "name"
```
###### is same as
```
a := "name"
```
###### `for` is Go's only looping construct - the following are basic types of `for`
```
i := 1
for i <= 3 {
	i = i+1;
}
```
```
for j := 7; j <= 9; j++ {
	fmt.Println(j);
}
```
###### `if-else` is simple like in other languages, there is no `ternary if` in Go
###### Arrays
```
var a [5]int // Array declaration that can hold 5 integers

b := [5]int{1, 2, 3, 4, 5} // Array declaration and initialization in the same line

var twoD [2][3]int // 2D array declaration for
```
