package main

import "fmt"

func main() {
	var a [5]int
	fmt.Println("emp: ", a)

	fmt.Println("len: ", len(a))

	b := [5]int{1, 3, 5, 6, 90}
	fmt.Println(b)

	var twoD [2][3]int
	for i:=0; i<2; i++ {
		for j:=0; j<3; j++ {
			twoD[i][j] = i+j
		}
	}
	fmt.Println("2D: ", twoD)
}
